import React, { Component } from "react";
import Axios from "axios";
import {} from 'reactstrap';

class Formr extends Component {
  //Buat state untuk menampung data yang diambil dari database
  state = {
    data: [],
    edit: [],
    buttonedit: false,
    name: "",
    job: "",
    idgan: ""
  };

  // START OFFUNCTION GETDATA
  //Fungsi getData untuk mengambil data pada server
  getData = () => {
    Axios.get("http://localhost:2000/api/data/") //Diambil dari server nedb kita
      .then(res => {
        //Menampung data yang diambil dari database ke state data
        console.log(res)
        this.setState({
          data: res.data
        });
      });
  };
  // END OF FUNCTION GET DATA

  //Membuat Fungsi Post
  postData = () => {
    Axios.post("http://localhost:2000/api/data/", {
      name: this.state.name,
      job: this.state.job
    }).then(res => {
      this.setState({
        name: "",
        job: ""
      });
      this.getData();
    });
  };

  putData = id => {
    // id.preventDefault();
    Axios.put(`http://localhost:2000/api/data/${id}`, {
      name: this.state.name,
      job: this.state.job
    }).then(res => {
      this.setState({
        name: "",
        job: "",
        buttonedit: false
      });
      this.getData(); } );
    
  };

  //   putData = id => {
  //     Axios.put(`http://localhost:8000/api/data/${id}`, {
  //       name: this.state.name,
  //       job: this.state.job
  //     }).then(res => {
  //       this.setState({
  //         name: "",
  //         job: "",
  //         buttonedit: false
  //       });
  //       this.getData();
  //     });
  //   };

  //Buat fungsi delete database
  deleteData = _id => {
    Axios.delete(`http://localhost:2000/api/data/${_id}`).then(res => {
      this.getData();
    });
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  getDataEdit = _id => {
    Axios.get(`http://localhost:2000/api/data/${_id}`).then(res => {
      console.log(res.data, "ini dataaaa");
      this.setState({
        name: res.data[0].name,
        job: res.data[0].job,
        buttonedit: true,
        idgan: res.data[0]._id
      });
    });
  };

  //Menjalankan fungsi getData
  componentDidMount() {
    this.getData();
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col col-lg-6">
              {/* <form> */}
                <div className="form-group" >
                  <label htmlFor="">Name</label>
                  <input onChange={this.handleChange} value={this.state.name} className="form-control" id="name" name="name" type="text"/>
                </div>
                <div className="form-group" >
                  <label htmlFor="">Job</label>
                  <input onChange={this.handleChange} value={this.state.job} className="form-control" id="job" name="job" type="text"/>
                </div>

                 {this.state.buttonedit ? (
                    <div>
                      <button onClick={() => { this.putData(this.state.idgan); }}> Edit </button>
                      <button onClick={() => { this.setState({
                            buttonedit: false,
                            title: "",
                            desc: ""
                          }); }}> Cancel
                      </button>
                    </div>
          ) : (
            <button onClick={()=>{
              this.postData();
            }} className="btn btn-success">Send</button>
          )}
              {/* </form> */}
          </div>
        </div>

        {
          this.state.data.map(datum=>{
            return(
              <div className="my-3" >
                <h2>Name : {datum.name}</h2>
                <h3>Job : {datum.job}</h3>
                <button onClick={()=>{ this.deleteData(datum._id); }} >Delete</button>
                <button onClick={()=>{ this.getDataEdit(datum._id); }} >Edit</button>
                <hr/>
              </div>
            )
          })
        }
        
      </div>
    );
  }
}

export default Formr
