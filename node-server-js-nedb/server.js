// inisialisai untuk mengunakan modul yang sebelumnya sudah di install
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const Datastore = require('nedb');

// membuat varibale untuk express()
const app = express();

app.use(cors()); //ini untuk menggunakan cors kita
app.use(bodyParser.json());//ini biar kita g gunain lagi parser atau signify untuk ngubah json ke object atau sebaliknya

// membuat database 
const db = new Datastore({filename: '.data/db.json', autoload: true});

// http://expressjs.com/en/starter/basic-routing.html
app.get('/', function(request,response){

    // proses
    response.send('ok')
});

//list data
app.get('/api/data', (req, res)=>{
    db.find({}, function(err,docs){
        res.send(docs)
    });
});

// insert data
app.post('/api/data/', (req,res)=>{

    console.log(req,res);

    const data = {
        "name": req.body.name,
        "job" : req.body.job
    }

    db.insert(data, (err, docs)=>{
        // if(err){
        //     res.send('error');
        //     return;
        // }

        res.send(docs);
    });
});

// ============= update data ==================
app.put('/api/data/:id', (req,res)=>{
    console.log(req.body);

    const data = {
        "name": req.body.name,
        "job" : req.body.job
    }

    db.update({ _id: req.params.id }, data, {}, function(err,numReplaced){

        res.send('update completed')
    });
});

var listener  = app.listen(2000, function(){
    console.log('Your app is listening on port  : ' + listener.address().port);
})

// ============= delete data ==================
app.delete('/api/data/:id',(req,res) =>{
    db.remove({_id: req.params.id}, {}, function(req,numReplaced){
        res.send('Delete Success');
    });
});

// ============= detail data ===============
app.get('/api/data/:id', (req,res)=>{
    db.find({_id: req.params.id}, function(err,docs){
        res.send(docs)
    });
});